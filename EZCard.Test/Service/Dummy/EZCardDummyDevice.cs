﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using EZCard.Models.EZCard;
using EZCard.Services.EZCard;
namespace EZCard.Test.Service.Dummy
{
    public class EZCardDummyDevice : EZCardDevice
    {
        protected string _lastSend;
        public override IEZCardDevice Send(TM msg)
        {
            this._lastSend = msg.TRANS.T0300;
            return this;
        }
        public override async Task<IEnumerable<EDC>> GetResponse()
        {
            return await Task.Run(()=>{
                var res = new List<EDC>();
                string xml = "";
                this._lastSend = "";
                switch (this._lastSend)
                {
                    //交易
                    case "811599":
                        xml = "<EDC><TRANS><T0100>0110</T0100><T0300>811599</T0300><T0200>949448773</T0200><T0212>0</T0212><T0213>00</T0213><T0214>05</T0214><T0400>200</T0400><T0408>2469</T0408><T0410>2669</T0410><T1101>001003</T1101><T1200>105144</T1200><T1201>144330</T1201><T1300>20190627</T1300><T1301>20140520</T1301><T1402>20191231</T1402><T3700>20190627057667</T3700><T3701>POS_TXN_NUM_011</T3701><T3800></T3800><T3900>0000</T3900><T4100>5C100D112700</T4100><T4101>8CD0DC00</T4101><T4102>0.0.0.0</T4102><T4103>000001120020300</T4103><T4104>E9FECC5D</T4104><T4200>00010001</T4200><T4210>0</T4210><T4800>00</T4800><T4801>005A00009072925C0B1E0000D81C00DC000000008CD0DC00000000000000000000000000000000000000000000</T4801><T4802>02</T4802><T4803>00</T4803><T4804>01</T4804><T5501>19061027</T5501><T5503>0000100001</T5503></TRANS></EDC>";
                        break;
                    //加值
                    case "811799":
                        break;
                    //退貨
                    case "851999":
                        break;
                    //餘額查詢
                    case "200000":
                        xml = "<EDC><TRANS><T0100>0110</T0100><T0300>200000</T0300><T0200>2011983328</T0200><T0212>1</T0212><T0213>08</T0213><T0214>00</T0214><T0410>7</T0410><T1402>20191231</T1402><T3900>0000</T3900><T4802>02</T4802><T4803>32</T4803><T4804>01</T4804></TRANS></EDC>";
                        break;
                    default:
                        xml = "<EDC><TRANS><T3900>6403</T3900><ErrMsg>餘額不足!!!錯誤代碼:6403</ErrMsg></TRANS></EDC>";
                        break;
                }
                res.Add(this.Deserialize(xml));
                this._latestReceive = res;
                return res;
            });
        }
    }
}
