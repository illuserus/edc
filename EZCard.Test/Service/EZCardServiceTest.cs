﻿using EZCard.Models.EZCard;
using EZCard.Services.EZCard;
using EZCard.Test.Service.Dummy;
using EZCard.Exceptions;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace EZCard.Test.Service
{
    public class EZCardServiceTest<T> where T: IEZCardService, new() 
    {
        protected IEZCardService _Service;
        public EZCardServiceTest()
        {
            this._Service = new T();
        }
        [Fact]
        public virtual void SignOn()
        {
            bool res = true;
            try
            {
                var info = new EZCardInfo()
                {

                };
                this._Service.SignOn(info);
            }
            catch
            {
                res = false;
            }
            Assert.True(res, "should be true");
        }

        [Fact]
        public async virtual void Pay()
        {
            bool res = true;
            try
            {
                var info = new TM();
                await this._Service.Pay("testOrder",1000);
            }
            catch(EZCardServiceException ex)
            {
                res = true;
            }
            catch(Exception ex)
            {
                res = false;
            }
            Assert.True(res);
        }

        [Fact]
        public async virtual void Balance()
        {
            bool res = true;
            try
            {
                await this._Service.Balance();
            }
            catch
            {
                res = false;
            }
            Assert.True(res);
        }

        [Fact]
        public async virtual void Reject()
        {
            bool res = true;
            try
            {
                await this._Service.Reject("kll", 100);
            }
            catch
            {
                res = false;
            }
            Assert.True(res);
        }

        [Fact]
        public async virtual void Charge()
        {
            bool res = true;
            try
            {
                await this._Service.Charge("tddr", 1000);
            }
            catch
            {
                res = false;
            }
            Assert.True(res);
        }
        [Fact]
        public async virtual void CheckOut()
        {
            bool res = true;
            try
            {
                await this._Service.CheckOut();
            }
            catch
            {
                res = false;
            }
            Assert.True(res);
        }


    }

    public class EZCardService_DummyDeviceTest : EZCardServiceTest <EZCardService>
    {
        public EZCardService_DummyDeviceTest()
        {
            this._Service = new EZCardService(new EZCardDummyDevice());
        }
    }

    public class EZCardServiceTest : EZCardServiceTest<EZCardService>
    {
        public EZCardServiceTest()
        {
            this._Service = new EZCardService(new EZCardDevice(1));
        }
    }
}
