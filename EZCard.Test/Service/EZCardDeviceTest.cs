﻿using EZCard.Models.EZCard;
using EZCard.Services.EZCard;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace EZCard.Test.Service
{
    public class EZCardDeviceTest<T> where T: EZCardDevice,new()
    {
        protected IEZCardDevice _device;
        public EZCardDeviceTest()
        {
            this._device = new T();
        }
        [Fact]
        public void Send()
        {
            bool res = true;
            string errorMsg="";
            try
            {
                var msg = new TM();
                this._device.Send(msg);
            }
            catch(Exception ex)
            {
                res = false;
                errorMsg = ex.Message;
            }
            Assert.True(res,errorMsg);
        }
        [Fact]
        public void GetResponse()
        {

        }
    }
}
