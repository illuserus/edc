﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EZCard.Exceptions
{

    public class EZCardException : Exception
    {
        private EZCardException() { }
        public EZCardException(string msg) : base(msg) { }
        public EZCardException(string msg, Exception exception) : base(msg, exception) { }
    }
    /// <summary>
    /// 悠遊卡服務層Exception
    /// </summary>
    public class EZCardServiceException : EZCardException
    {
        public EZCardServiceException(string msg) : base(msg) { }
        public EZCardServiceException(string msg, Exception exception) : base(msg, exception) { }
    }
    /// <summary>
    /// 悠遊卡硬體控制層Exception
    /// </summary>
    public class EZCardDeviceException : EZCardException
    {
        public EZCardDeviceException(string msg) : base(msg) { }
        public EZCardDeviceException(string msg, Exception exception) : base(msg, exception) { }
    }

}
