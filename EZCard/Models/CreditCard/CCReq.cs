﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EZCard.Models.CreditCard
{
    /// <summary>
    /// 銀聯卡請求電文/銀聯卡終端機回覆格式(共用格式)
    /// </summary>
    public class CCReq
    {
        public string ECRIndicator { get; set; }
        public string ECRVersionDate { get; set; }
        public string TransTypeIndicator { get; set; }
        public string TransType { get; set; }
        public string CUP_SP_ESVCIndicator { get; set; }
        public string HostID { get; set; }
        public string ReceiptNo { get; set; }
        public string CardNo { get; set; }
        public string CardExpireDate { get; set; }
        public string TransAmount { get; set; }
        public string TransDate { get; set; }
        public string TransTime { get; set; }
        public string ApprovalNo { get; set; }
        public string WaveCardIndicator { get; set; }
        public string ECRResponseCode { get; set; }
        public string MerchantID { get; set; }
        public string TerminalID { get; set; }
        public string ExpAmount { get; set; }
        public string StoreId { get; set; }
        public string Installment_RedeemIndicator { get; set; }
        public string RDMPaidAmt { get; set; }
        public string RDMPoint { get; set; }
        public string PointOfBalance { get; set; }
        public string RedeemAmt { get; set; }
        public string InstallmentPeriod { get; set; }
        public string DownPaymentAmount_ESVCBalanceBefore { get; set; }
        public string FormalityFee_ESVCAutoloadAmount { get; set; }
        public string CardType { get; set; }
        public string BatchNo { get; set; }
        public string StartTransType { get; set; }
        public string MPFlag { get; set; }
        public string SPIssueID { get; set; }
        public string SP_CC_ESVCOriginDate { get; set; }
        public string SPOriginRRN_CCOriginRRN_ATS { get; set; }
        public string PayItem { get; set; }
        public string CardNo_HashValue { get; set; }
        public string MPResponseCode { get; set; }
        public string ASMAwardFlag { get; set; }
        public string MCPIndicator { get; set; }
        public string BankCode { get; set; }
        public string Reserved { get; set; }

    }
}
