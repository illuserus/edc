﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EZCard.Models.EZCard
{
    /// <summary>
    /// 接收
    /// </summary>
    public class EDC
    {
        public EDC()
        {
            this.TRANS = new TRANS();
        }
        public TRANS TRANS { get; set; }
    }
}
