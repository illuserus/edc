﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EZCard.Models.EZCard
{
    public class T5595
    {
        /// <summary>
        /// 放入字串"PWD"
        /// </summary>
        public string T559502 { get; set; }
        /// <summary>
        /// 密碼:為營業人統編末4碼，測試環境5909為悠遊卡公司統編末4碼
        /// </summary>
        public string T559503 { get; set; }
    }
}
