﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EZCard.Models.EZCard
{
    public enum ProcessingCode
    {
        /// <summary>
        /// 初始化
        /// </summary>
        SignOn = 881999,
        /// <summary>
        /// 交易(購買)
        /// </summary>
        Pay = 811599,
        /// <summary>
        /// 加值
        /// </summary>
        Charge = 811799,
        /// <summary>
        /// 取消交易
        /// </summary>
        CancelPurchase = 823899,
        /// <summary>
        /// 退貨
        /// </summary>
        Reject = 851999,
        /// <summary>
        /// 查詢交易狀態
        /// </summary>
        CheckPurchase = 999999,
        /// <summary>
        /// 查詢餘額
        /// </summary>
        Balance = 200000,
        /// <summary>
        /// 結帳
        /// </summary>
        CheckOut = 950007,
    }
}
