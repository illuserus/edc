﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EZCard.Models.EZCard
{
    public class EZCardInfo
    {
        /// <summary>
        /// 統編
        /// </summary>
        public string BAN { get; set; }
        public string CashierID { get; set; }
        public string EmpID { get; set; }
    }
}
