﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EZCard.Models.EZCard
{

    public class TRANS
    {
        /// <summary>
        /// Message Type Id
        /// </summary>
        public string T0100 { get; set; }
        /// <summary>
        /// Card Physical Id
        /// </summary>
        public string T0200 { get; set; }
        /// <summary>
        /// PID(Purse Id)
        /// </summary>
        public string T0211 { get; set; }
        /// <summary>
        /// 悠遊卡 auto-load flag
        /// </summary>
        public string T0212 { get; set; }
        /// <summary>
        /// Card Ttype
        /// </summary>
        public string T0213 { get; set; }
        /// <summary>
        /// Personal Profile
        /// </summary>
        public string T0214 { get; set; }
        /// <summary>
        /// Processing Code
        /// </summary>
        public string T0300 { get; set; }
        /// <summary>
        /// Amount
        /// </summary>
        public string T0400 { get; set; }
        /// <summary>
        /// 交易後餘額
        /// </summary>
        public string T0408 { get; set; }
        /// <summary>
        /// Single auto-load-transaction amount
        /// </summary>
        public string T0409 { get; set; }
        /// <summary>
        /// 交易前金額
        /// </summary>
        public string T0410 { get; set; }
        /// <summary>
        /// TM Serial Number
        /// </summary>
        public string T1100 { get; set; }
        /// <summary>
        /// 收銀機交易序號
        /// </summary>
        public string T1101 { get; set; }
        /// <summary>
        /// 本地交易時間
        /// </summary>
        public string T1200 { get; set; }
        /// <summary>
        /// Time
        /// </summary>
        public string T1201 { get; set; }
        /// <summary>
        /// 本地交易日期
        /// </summary>
        public string T1300 { get; set; }
        /// <summary>
        /// Date
        /// </summary>
        public string T1301 { get; set; }
        /// <summary>
        /// Card expire date
        /// </summary>
        public string T1400 { get; set; }
        /// <summary>
        /// 卡片有效日期
        /// </summary>
        public string T1402 { get; set; }
        /// <summary>
        /// 卡片有效日期(展延後)
        /// </summary>
        public string T1403 { get; set; }
        /// <summary>
        /// Retrieval Reference Number
        /// </summary>
        public string T3700 { get; set; }
        /// <summary>
        /// 收銀機交易編號
        /// </summary>
        public string T3701 { get; set; }
        /// <summary>
        /// Authorization Identification Response
        /// </summary>
        public string T3800 { get; set; }
        /// <summary>
        /// Response Code
        /// </summary>
        public string T3900 { get; set; }
        /// <summary>
        /// New Device Id
        /// </summary>
        public string T4100 { get; set; }
        /// <summary>
        /// Device Id
        /// </summary>
        public string T4101 { get; set; }
        /// <summary>
        /// 端末設備目前IP
        /// </summary>
        public string T4102 { get; set; }
        /// <summary>
        /// 端末設備機器序號
        /// </summary>
        public string T4103 { get; set; }
        /// <summary>
        /// Reader Id
        /// </summary>
        public string T4104 { get; set; }
        /// <summary>
        /// New SP Id
        /// </summary>
        public string T4200 { get; set; }
        /// <summary>
        /// New Location Id
        /// </summary>
        public string T4210 { get; set; }
        /// <summary>
        /// Purse version number
        /// </summary>
        public string T4800 { get; set; }
        /// <summary>
        /// TM Location ID(特約商店分店號)
        /// </summary>
        public string T5503 { get; set; }
        /// <summary>
        /// TM ID(收銀機機號)
        /// </summary>
        public string T5504 { get; set; }
        /// <summary>
        /// TM agent number(收銀員代號)
        /// </summary>
        public string T5510 { get; set; }
        /// <summary>
        /// 驗證資訊
        /// </summary>
        public T5595 T5595 { get; set; }
        /// <summary>
        /// 錯誤訊息
        /// </summary>
        public string ErrMsg { get; set; }
    }
}
