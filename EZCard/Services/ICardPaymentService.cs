﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace EZCard.Services
{
    public interface ICardPaymentService<TMsg,TResp> 
        where TMsg : class,new()
        where TResp : class,new()
    {
        /// <summary>
        /// 一般交易
        /// </summary>
        /// <param name="OrderId"></param>
        /// <param name="Amount"></param>
        /// <returns></returns>
        Task<TResp> Pay(string OrderId, int Amount);

        /// <summary>
        /// 加值
        /// </summary>
        /// <param name="Amount"></param>
        Task<TResp> Charge(string OrderId, int Amount);
        /// <summary>
        /// 退貨
        /// </summary>
        /// <returns></returns>
        Task<TResp> Reject(string OrderId, int Amount);
        /// <summary>
        /// 餘額查詢
        /// </summary>
        Task<TResp> Balance();
        /// <summary>
        /// 結帳
        /// </summary>
        Task<TResp> CheckOut();
    }
}
