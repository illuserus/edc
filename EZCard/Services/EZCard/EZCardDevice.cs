﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Ports;
using System.Text;
using System.Linq;
using System.Xml.Serialization;
using EZCard.Exceptions;
using EZCard.Models.EZCard;
using System.Xml;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace EZCard.Services.EZCard
{
    public class EZCardDevice : IEZCardDevice
    {
        public enum SIG
        {
            STX = 0x2,
            ETX = 0x3,
            ACK = 0x6,
            NAK = 0x15
        }
        protected string _latestSend;
        protected TM _latestMsg;
        protected IEnumerable<EDC> _latestReceive;
        protected SerialPort _serialPort;
        protected XmlSerializer _xmlSerializer;

        public TM LatestMsg => this._latestMsg;

        public string LatestSend => this._latestSend;

        public IEnumerable<EDC> LatestReceive => this._latestReceive;
#if DEBUG
        public EZCardDevice()
        {

        }
#endif
        public EZCardDevice(int comportNo)
        {
            this._serialPort = new SerialPort
            {
                PortName = "COM" + comportNo.ToString(),
                BaudRate = 115200,
                DataBits = 8,
                ReadTimeout = 20000
            };
            //ackTimeout = 2000
            //cntOfRetrySendMsg = 3
        }

        public EZCardDevice(SerialPort port)
        {
            this._serialPort = port;
        }
        protected virtual string Serialize(TM msg)
        {
            XmlSerializer ser = new XmlSerializer(typeof(TM));
            StringBuilder sb = new StringBuilder();
            StringWriter writer = new StringWriter(sb);
            ser.Serialize(writer, msg);
            return sb.ToString();
        }

        protected virtual EDC Deserialize(string xmlstring)
        {
            XmlSerializer ser = new XmlSerializer(typeof(EDC));
            using (var sr = new StringReader(xmlstring))
            {
                return (EDC)ser.Deserialize(sr);
            }
        }

        private void WrapException(Exception ex)
        {
            throw new EZCardDeviceException("悠遊卡硬體控制層出現例外", ex);
        }

        public virtual IEZCardDevice Send(TM msg)
        {
            try
            {
                string strMsg = SIG.STX.ToString() + this.Serialize(msg) + SIG.ETX.ToString();
                this._serialPort.Write(strMsg);
                this._latestSend = strMsg;
                this._latestMsg = msg;
            }
            catch (Exception ex)
            {
                this.WrapException(ex);
            }
            return this;
        }

        protected virtual IEZCardDevice ReSend()
        {
            this._serialPort.Write(this.LatestSend);
            return this;
        }

        public virtual async Task<IEnumerable<EDC>> GetResponse()
        {
            try
            {
                SIG receiveSIG = (SIG)this._serialPort.ReadByte();
                switch (receiveSIG)
                {
                    case SIG.ACK:
                        this.UnpackMessage().ResponseACK();
                        break;
                    case SIG.NAK:
                    default:
                        await this.ReSend().GetResponse();
                        break;
                }
            }
            catch (Exception ex)
            {
                this.WrapException(ex);
            }
            return this.LatestReceive;
        }
        protected virtual EZCardDevice ResponseACK()
        {
            string msg = SIG.ACK.ToString();
            this._serialPort.Write(msg);
            return this;
        }

        protected virtual EZCardDevice UnpackMessage()
        {
            this._latestReceive = null;
            var receive = new List<Byte>(this._serialPort.BytesToRead);
            string receiveString = System.Text.Encoding.UTF8.GetString(receive.ToArray());
            var Matches = Regex.Matches(receiveString, @"^2(?<xml>\S*)3$");
            var res = new List<EDC>();
            foreach (Match item in Matches)
            {
                var edc = this.Deserialize(item.Groups["xml"].ToString());
                res.Add(edc);
            }
            this._latestReceive = res;
            return this;
        }
        public virtual void Dispose()
        {
            ////Maybe this style

            //if (this._serialPort.IsOpen)
            //{
            //    this._serialPort.Close();
            //}

            ////or this
            this._serialPort.Dispose();
        }
    }
}
