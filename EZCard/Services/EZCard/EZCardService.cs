﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO.Ports;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Security;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using EZCard.Exceptions;
using EZCard.Models.EZCard;
namespace EZCard.Services.EZCard
{

    public class EZCardService : IEZCardService
    {
        protected IEZCardDevice _device;
        protected EZCardInfo _info;
        protected virtual TM GetMsg(ProcessingCode code)
        {
            var msg = this.GetMsg();
            msg.TRANS.T0300 = ((int)code).ToString();
            return msg;
        }

        protected virtual TM GetMsg()
        {
            DateTime dtNow = DateTime.Now;
            var msg = new TM();
            msg.TRANS.T1201 = dtNow.ToString("HHmmss");
            msg.TRANS.T1301 = dtNow.ToString("yyyyMMdd");
            return msg;
        }
#if DEBUG
        public EZCardService()
        {
            this._device = new EZCardDevice(1);
        }
#endif

        public EZCardService(IEZCardDevice device)
        {
            this._device = device;
        }

        protected virtual async Task<EDC> Trade(ProcessingCode code,string OrderId,int Amount)
        {
            const string ReqMsgId = "0100";
            const string RespMsgId = "0110";
            var msg = this.GetMsg(code);
            msg.TRANS.T0100 = ReqMsgId;
            msg.TRANS.T0400 = Amount.ToString();
            msg.TRANS.T3701 = OrderId;
            var resp = await this._device.Send(msg).GetResponse();
            EDC res = null;
            try
            {
                res = resp.Where(x => x.TRANS.T3701 == OrderId && x.TRANS.T0100 == RespMsgId && x.TRANS.T0300==msg.TRANS.T0300).First();
            }
            catch (Exception ex)
            {
                res = null;
                this.WrapException(ex);
            }
            return res;
        }
        protected virtual async Task<EDC> Action(ProcessingCode code,string ReqMsgId,string RespMsgId)
        {
            var msg = this.GetMsg(code);
            msg.TRANS.T0100 = ReqMsgId;
            var resp = await this._device.Send(msg).GetResponse();
            EDC res = null;
            try
            {
                res = resp.Where(x => x.TRANS.T0300 == msg.TRANS.T0300 && x.TRANS.T0100 == RespMsgId).First();
            }
            catch (Exception ex)
            {
                res = null;
                this.WrapException(ex);
            }
            return res;
        }
        public virtual async Task<EDC> Pay(string OrderId, int Amount)
        {
            return await this.Trade( ProcessingCode.Pay,OrderId,Amount);
        }

        public async Task<bool> SignOn(EZCardInfo info)
        {
            const string MessageId = "0810";
            this._info = info;
            var msg = this.GetMsg(ProcessingCode.SignOn);
            msg.TRANS.T0100 = "0800";
            msg.TRANS.T1101 = "0";

            msg.TRANS.T5504 = this._info.CashierID;
            msg.TRANS.T5510 = this._info.EmpID;
            msg.TRANS.T5595 = new T5595()
            {
                T559502 = "PWD",
#if DEBUG
                T559503 = "5909"
#else
                T559503 = this._info.BAN.Substring(this._info.BAN.Length - 4)
#endif
            };
            bool res = true;
            try
            {
                var resp = await this._device.Send(msg).GetResponse();
                res = resp.Any(x => x.TRANS.T0100 == MessageId && x.TRANS.T0300 == msg.TRANS.T0300);
            }
            catch (Exception ex)
            {
                this.WrapException(ex);
                res = false;
            }
            return res;
        }

        public void WrapException(Exception ex)
        {
            var ErrMsg = this._device.LatestReceive.Where(x => !string.IsNullOrEmpty(x.TRANS.ErrMsg)).Select(x => x.TRANS.ErrMsg).ToList();
            if (ErrMsg.Count > 0)
            {
                throw new EZCardServiceException(string.Join(";", ErrMsg), ex);
            }
            else
            {
                throw new EZCardServiceException("悠遊卡服務層出現例外，請參照內部例外", ex);
            }
        }

        public void Dispose()
        {
            this._device.Dispose();
        }



        public async Task<EDC> Charge(string OrderId,int Amount)
        {
            return await this.Trade(ProcessingCode.Charge, OrderId, Amount);
        }

        public async Task<EDC> Reject(string OrderId,int Amount)
        {
            return await this.Trade(ProcessingCode.Reject, OrderId, Amount);
        }
        public async Task<EDC> Balance()
        {
            const string ReqMsgId = "0100";
            const string RespMsgId = "0110";
            return await this.Action(ProcessingCode.Balance, ReqMsgId,RespMsgId);
        }
        public async Task<EDC> CheckOut()
        {
            const string ReqMsgId = "0320";
            const string RespMsgId = "0330";
            return await this.Action(ProcessingCode.CheckOut, ReqMsgId,RespMsgId);
        }
    }


}
