﻿using EZCard.Exceptions;
using EZCard.Models.EZCard;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace EZCard.Services.EZCard
{
    public interface IEZCardService : IDisposable
    {
        /// <summary>
        /// 初始化裝置
        /// </summary>
        /// <param name="CashierID"></param>
        /// <param name="ID"></param>
        /// <param name="PW"></param>
        Task<bool> SignOn(EZCardInfo info);
        /// <summary>
        /// 付款
        /// </summary>
        /// <param name="Amount"></param>
        Task<EDC> Pay(string OrderId, int Amount);

        /// <summary>
        /// 加值
        /// </summary>
        /// <param name="Amount"></param>
        Task<EDC> Charge(string OrderId, int Amount);
        /// <summary>
        /// 退貨
        /// </summary>
        /// <returns></returns>
        Task<EDC> Reject(string OrderId, int Amount);
        /// <summary>
        /// 餘額查詢
        /// </summary>
        Task<EDC> Balance();
        /// <summary>
        /// 結帳
        /// </summary>
        Task<EDC> CheckOut();

        ///// <summary>
        ///// 取消交易
        ///// </summary>
        //void Cancel_Pay();
        ///// <summary>
        ///// 取消加值
        ///// </summary>
        //void Cancel_Deposit();


        ///// <summary>
        ///// 交易紀錄
        ///// </summary>
        //void TradeRecord();   
    }
}
