﻿using EZCard.Models.EZCard;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace EZCard.Services.EZCard
{
    public interface IEZCardDevice : IDisposable
    {
        TM LatestMsg { get; }
        string LatestSend { get; }
        IEnumerable<EDC> LatestReceive { get; }
        IEZCardDevice Send(TM msg);
        Task<IEnumerable<EDC>> GetResponse();
    }
}
