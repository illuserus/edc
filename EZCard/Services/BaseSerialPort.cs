﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Text;

namespace EZCard.Services
{
    public abstract class BaseSerialPort<T> : IDisposable where T: class,new() 
    {
        protected string _latestSend;
        protected T _latestMsg;
        protected IEnumerable<T> _latestReceive;
        protected SerialPort _serialPort;

        public T LatestMsg => this._latestMsg;

        public string LatestSend => this._latestSend;

        public IEnumerable<T> LatestReceive => this._latestReceive;
        public BaseSerialPort(int PortNo,int BaudRate,int DataBit,int ReadTimeout)
        {
            this._serialPort = new SerialPort()
            {
                PortName = "COM" + PortNo.ToString(),
                BaudRate = BaudRate,
                DataBits = DataBit,
                ReadTimeout = ReadTimeout
            };
        }

        public BaseSerialPort(SerialPort port)
        {
            this._serialPort = port;
        }

        protected abstract string Serialize(T Msg);
        protected abstract T Deserialize(string rawString);
        public void Dispose()
        {
            this._serialPort.Dispose();
        }
    }
}
