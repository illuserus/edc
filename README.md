###金流模組
-------------------
>實作各常見的金流模組

>目前無編輯的專案，請先行卸載
####Einvorment:
+   .Net Core 2.2

>SDK https://dotnet.microsoft.com/download/dotnet-core/2.2
###Sub Project:
-------------------
>包含模組

+	EZCardService(悠遊卡付款邏輯模組)
+	EZCardDevice(悠遊卡終端機硬體控制)
+	CreditCardService(銀聯卡付款邏輯模組)
+	CreditCardDevice(銀聯卡終端機硬體控制)

>子專案
+	EZCard.Test				- 單元測試專案
+	EZCard.App				- 實機測試專案

###TODO
-------------------
